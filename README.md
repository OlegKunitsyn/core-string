# Core String
GnuCOBOL core library with string functions

<p align="center">
  <img src="https://github.com/OlegKunitsyn/core-string/workflows/Docker%20Image%20CI/badge.svg" />
</p>

# Documentation
See coboldoc [documentation](https://github.com/OlegKunitsyn/core-string/tree/master/coboldoc).
